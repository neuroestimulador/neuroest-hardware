# Estimulador por pulsos de corrente

Esse hardware é um sistema que converte os pulsos de tensão gerados pelo microcontrolador em pulsos de corrente. Para tanto, é necessário duas entradas invertidas entre si para gerar um sinal bipolar (pulsos positivos e negativos). Assim, o sistema consiste de um inversor de pulsos e uma fonte de corrente controlada por tensão (VCCS - Voltage Controlled Current Source) bipolar, de alta precisão e boa resposta em frequência . Assim, ela converte sinais de tensão em sinais de corrente, podendo variar a proporção de ganho de transimpedância (relação de tensão de entrada por corrente de saída). 
Uma vez que sejam gerados os pulsos de corrente, pode-se realizar a aplicação em tecidos neuronais através de microeletrodos bipolares. 

As placas para funcionamento do sistema foram desenvolvidas em [KiCAD](http://kicad-pcb.org/).

---

# Diagrama de blocos do sistema

![Diagrama de blocos do sistema](/Imagens/neuroest_blocos.png)

Maiores informações do sistema presentes na [Wiki](https://git.cta.if.ufrgs.br/neuroestimulador/neuroestimulador-hardware/wikis/home) do projeto.

## Especificações

* Corrente de saída regulável entre 1uA e 110mA 
* Slew-rate de corrente de saída: 10A/ms 
* Compliância: 10V 
* Impedância de entrada: 2G ohms
* Tensão dos pulsos de entrada: ~5V

## Guia de repositório

*  [Arquivos de KiCAD](https://git.cta.if.ufrgs.br/neuroestimulador/neuroestimulador-hardware/tree/master/KiCAD): arquivos de diagramas esquemáticos e PCBs do projeto desenvolvidos em KiCAD
*  [Lista de materiais (BOM)](https://git.cta.if.ufrgs.br/neuroestimulador/neuroestimulador-hardware/tree/master/Materiais-BOM): lista de materiais para fabricação do hardware
*  [Diagramas](https://git.cta.if.ufrgs.br/neuroestimulador/neuroestimulador-hardware/tree/master/Diagramas): diagramas esquemáticos dos circuitos do sistema em PDF
*  [GERBER](https://git.cta.if.ufrgs.br/neuroestimulador/neuroestimulador-hardware/tree/master/GERBER): arquivos GERBER para fabricação das placas de circuito impresso

## Licença
A presente documentação descreve hardware, firmware e software de código aberto.
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />O trabalho <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Neuroestimulador</span> de <a xmlns:cc="http://creativecommons.org/ns#" href="http://cta.if.ufrgs.br" property="cc:attributionName" rel="cc:attributionURL">Luís Eduardo Estradioto</a> está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons - Atribuição-CompartilhaIgual 4.0 Internacional</a>.