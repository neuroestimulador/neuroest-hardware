EESchema Schematic File Version 4
LIBS:alimentacao-cache
EELAYER 26 0
EELAYER END
$Descr User 13780 4724
encoding utf-8
Sheet 1 1
Title "Fonte de alimentação"
Date "2019-08-10"
Rev "1"
Comp "CTA-UFRGS"
Comment1 "Fonte de alimentação simétrica"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 11400 11850 0    79   ~ 0
Fonte de Corrente Bipolar para Estimulador Neuronal\n
$Comp
L Connector_Generic:Conn_01x01 J1
U 1 1 5CC3EAC7
P 1900 1300
F 0 "J1" H 1820 1075 50  0000 C CNN
F 1 "TRAFO +" H 1820 1166 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.2mm" H 1900 1300 50  0001 C CNN
F 3 "~" H 1900 1300 50  0001 C CNN
	1    1900 1300
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5CC3EEB7
P 1900 1650
F 0 "J2" H 1820 1425 50  0000 C CNN
F 1 "- TRAFO" H 1820 1516 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.2mm" H 1900 1650 50  0001 C CNN
F 3 "~" H 1900 1650 50  0001 C CNN
	1    1900 1650
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 5CC3F0CD
P 1900 2050
F 0 "J3" H 1820 1825 50  0000 C CNN
F 1 "GND TRAFO" H 1820 1916 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.2mm" H 1900 2050 50  0001 C CNN
F 3 "~" H 1900 2050 50  0001 C CNN
	1    1900 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 2800 4000 2800
Connection ~ 4000 2800
Wire Wire Line
	4000 2450 4000 2800
Wire Wire Line
	3850 2450 4000 2450
Wire Wire Line
	4000 1650 4000 1300
Wire Wire Line
	3850 1650 4000 1650
Connection ~ 4000 1300
Wire Wire Line
	3850 1300 4000 1300
$Comp
L Device:CP C4
U 1 1 5C93EAFA
P 4750 2600
F 0 "C4" H 4868 2646 50  0000 L CNN
F 1 "470uF" H 4868 2555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P5.00mm" H 4788 2450 50  0001 C CNN
F 3 "~" H 4750 2600 50  0001 C CNN
	1    4750 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C3
U 1 1 5C93E9F6
P 4750 1500
F 0 "C3" H 4868 1546 50  0000 L CNN
F 1 "470uF" H 4868 1455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P5.00mm" H 4788 1350 50  0001 C CNN
F 3 "~" H 4750 1500 50  0001 C CNN
	1    4750 1500
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4007 D4
U 1 1 5C93E8EA
P 3700 2800
F 0 "D4" H 3700 3016 50  0000 C CNN
F 1 "1N4001" H 3700 2925 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3700 2625 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 3700 2800 50  0001 C CNN
	1    3700 2800
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4007 D3
U 1 1 5C93E7F0
P 3700 2450
F 0 "D3" H 3700 2666 50  0000 C CNN
F 1 "1N4001" H 3700 2575 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3700 2275 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 3700 2450 50  0001 C CNN
	1    3700 2450
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4007 D2
U 1 1 5C93E638
P 3700 1650
F 0 "D2" H 3700 1434 50  0000 C CNN
F 1 "1N4001" H 3700 1525 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3700 1475 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 3700 1650 50  0001 C CNN
	1    3700 1650
	-1   0    0    1   
$EndComp
$Comp
L Diode:1N4007 D1
U 1 1 5C93E468
P 3700 1300
F 0 "D1" H 3700 1084 50  0000 C CNN
F 1 "1N4001" H 3700 1175 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3700 1125 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 3700 1300 50  0001 C CNN
	1    3700 1300
	-1   0    0    1   
$EndComp
$Comp
L regul:LM317_3PinPackage U1
U 1 1 5D31688E
P 5400 1300
F 0 "U1" H 5400 1542 50  0000 C CNN
F 1 "LM317" H 5400 1451 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5400 1550 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm317.pdf" H 5400 1300 50  0001 C CNN
	1    5400 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 1300 4300 1300
Wire Wire Line
	4750 1350 4750 1300
Connection ~ 4750 1300
Wire Wire Line
	4750 1300 5100 1300
Wire Wire Line
	4750 2800 4750 2750
$Comp
L Device:CP C1
U 1 1 5D318212
P 4300 1500
F 0 "C1" H 4418 1546 50  0000 L CNN
F 1 "1000uF" H 4418 1455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 4338 1350 50  0001 C CNN
F 3 "~" H 4300 1500 50  0001 C CNN
	1    4300 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5D318274
P 4300 2600
F 0 "C2" H 4418 2646 50  0000 L CNN
F 1 "1000uF" H 4418 2555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 4338 2450 50  0001 C CNN
F 3 "~" H 4300 2600 50  0001 C CNN
	1    4300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 1350 4300 1300
Connection ~ 4300 1300
Wire Wire Line
	4300 1300 4750 1300
$Comp
L Diode:1N4007 D5
U 1 1 5D3198A0
P 5400 850
F 0 "D5" H 5400 1066 50  0000 C CNN
F 1 "1N4001" H 5400 975 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5400 675 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 5400 850 50  0001 C CNN
	1    5400 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1300 5950 1300
Wire Wire Line
	5950 1300 5950 850 
Wire Wire Line
	5950 850  5550 850 
Wire Wire Line
	5250 850  4750 850 
Wire Wire Line
	4750 850  4750 1300
$Comp
L Device:CP C5
U 1 1 5D31A868
P 5400 1800
F 0 "C5" H 5518 1846 50  0000 L CNN
F 1 "22uF" H 5518 1755 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 5438 1650 50  0001 C CNN
F 3 "~" H 5400 1800 50  0001 C CNN
	1    5400 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2800 4300 2800
Wire Wire Line
	4300 2750 4300 2800
Connection ~ 4300 2800
Wire Wire Line
	4300 2800 4750 2800
Wire Wire Line
	4300 1650 4300 2050
Wire Wire Line
	4750 1650 4750 2050
Wire Wire Line
	4300 2050 4750 2050
Connection ~ 4300 2050
Wire Wire Line
	4300 2050 4300 2450
Connection ~ 4750 2050
Wire Wire Line
	4750 2050 4750 2450
Wire Wire Line
	5400 1600 5400 1650
Wire Wire Line
	4750 2050 5400 2050
Wire Wire Line
	5400 2050 5400 1950
$Comp
L Diode:1N4007 D6
U 1 1 5D31DEA5
P 5950 1500
F 0 "D6" H 5950 1716 50  0000 C CNN
F 1 "1N4001" H 5950 1625 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5950 1325 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 5950 1500 50  0001 C CNN
	1    5950 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 1650 5400 1650
Connection ~ 5400 1650
Wire Wire Line
	5950 1350 5950 1300
Connection ~ 5950 1300
$Comp
L Device:R R1
U 1 1 5D31FB2E
P 6300 1500
F 0 "R1" H 6370 1546 50  0000 L CNN
F 1 "270" H 6370 1455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6230 1500 50  0001 C CNN
F 3 "~" H 6300 1500 50  0001 C CNN
	1    6300 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1300 6300 1300
Wire Wire Line
	6300 1300 6300 1350
Wire Wire Line
	6300 1650 5950 1650
Connection ~ 5950 1650
$Comp
L Device:R_POT_TRIM RV1
U 1 1 5D320A2C
P 6300 1850
F 0 "RV1" H 6231 1804 50  0000 R CNN
F 1 "5k" H 6231 1895 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 6300 1850 50  0001 C CNN
F 3 "~" H 6300 1850 50  0001 C CNN
	1    6300 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6150 1850 5950 1850
Wire Wire Line
	5950 1850 5950 1650
Wire Wire Line
	6300 2000 6300 2050
Wire Wire Line
	6300 2050 5400 2050
Connection ~ 5400 2050
$Comp
L regul:LM337_TO220 U2
U 1 1 5D3218DB
P 5400 2800
F 0 "U2" H 5400 2650 50  0000 C CNN
F 1 "LM337" H 5400 2559 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5400 2600 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm337-n.pdf" H 5400 2800 50  0001 C CNN
	1    5400 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2800 5100 2800
Connection ~ 4750 2800
$Comp
L Device:CP C6
U 1 1 5D321FE7
P 5400 2250
F 0 "C6" H 5518 2296 50  0000 L CNN
F 1 "22uF" H 5518 2205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 5438 2100 50  0001 C CNN
F 3 "~" H 5400 2250 50  0001 C CNN
	1    5400 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2500 5400 2450
Wire Wire Line
	5400 2100 5400 2050
$Comp
L Diode:1N4007 D7
U 1 1 5D32302F
P 5400 3250
F 0 "D7" H 5400 3466 50  0000 C CNN
F 1 "1N4001" H 5400 3375 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5400 3075 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 5400 3250 50  0001 C CNN
	1    5400 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5250 3250 4750 3250
Wire Wire Line
	4750 3250 4750 2800
$Comp
L Diode:1N4007 D8
U 1 1 5D3266B2
P 5950 2650
F 0 "D8" H 5950 2866 50  0000 C CNN
F 1 "1N4001" H 5950 2775 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5950 2475 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 5950 2650 50  0001 C CNN
	1    5950 2650
	0    1    1    0   
$EndComp
Connection ~ 5400 2450
Wire Wire Line
	5400 2450 5400 2400
$Comp
L Device:R R2
U 1 1 5D328E99
P 6300 2650
F 0 "R2" H 6370 2696 50  0000 L CNN
F 1 "270" H 6370 2605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6230 2650 50  0001 C CNN
F 3 "~" H 6300 2650 50  0001 C CNN
	1    6300 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 2800 6300 2800
Wire Wire Line
	5400 2450 5950 2450
Wire Wire Line
	5950 2500 5950 2450
Wire Wire Line
	5700 2800 5950 2800
Connection ~ 5950 2800
Wire Wire Line
	5950 2450 6300 2450
Wire Wire Line
	6300 2450 6300 2500
Connection ~ 5950 2450
$Comp
L Device:R_POT_TRIM RV2
U 1 1 5D32FFF7
P 6300 2250
F 0 "RV2" H 6231 2204 50  0000 R CNN
F 1 "5k" H 6231 2295 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 6300 2250 50  0001 C CNN
F 3 "~" H 6300 2250 50  0001 C CNN
	1    6300 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6300 2100 6300 2050
Connection ~ 6300 2050
Wire Wire Line
	6150 2250 5950 2250
Wire Wire Line
	5950 2250 5950 2450
Wire Wire Line
	5950 2800 5950 3250
Wire Wire Line
	5550 3250 5950 3250
$Comp
L Device:CP C7
U 1 1 5D339F13
P 6650 1700
F 0 "C7" H 6768 1746 50  0000 L CNN
F 1 "10uF" H 6768 1655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 6688 1550 50  0001 C CNN
F 3 "~" H 6650 1700 50  0001 C CNN
	1    6650 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C8
U 1 1 5D33ABC7
P 6650 2450
F 0 "C8" H 6768 2496 50  0000 L CNN
F 1 "10uF" H 6768 2405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 6688 2300 50  0001 C CNN
F 3 "~" H 6650 2450 50  0001 C CNN
	1    6650 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 2050 6650 2050
Wire Wire Line
	6650 2050 6650 2300
Wire Wire Line
	6650 2600 6650 2800
Wire Wire Line
	6650 2800 6300 2800
Connection ~ 6300 2800
Wire Wire Line
	6300 1300 6650 1300
Wire Wire Line
	6650 1300 6650 1550
Connection ~ 6300 1300
Wire Wire Line
	6650 1850 6650 2050
Connection ~ 6650 2050
$Comp
L regul:L7905 U4
U 1 1 5D33F013
P 7450 2800
F 0 "U4" H 7450 2650 50  0000 C CNN
F 1 "L7905" H 7450 2559 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7450 2600 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/c9/16/86/41/c7/2b/45/f2/CD00000450.pdf/files/CD00000450.pdf/jcr:content/translations/en.CD00000450.pdf" H 7450 2800 50  0001 C CNN
	1    7450 2800
	1    0    0    -1  
$EndComp
$Comp
L regul:L7805 U3
U 1 1 5D33F21D
P 7450 1300
F 0 "U3" H 7450 1542 50  0000 C CNN
F 1 "L7805" H 7450 1451 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7475 1150 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 7450 1250 50  0001 C CNN
	1    7450 1300
	1    0    0    -1  
$EndComp
Connection ~ 6650 1300
Wire Wire Line
	7450 1600 7450 2050
Wire Wire Line
	7450 2050 7050 2050
Wire Wire Line
	7450 2050 7450 2500
Connection ~ 7450 2050
Wire Wire Line
	7150 2800 7050 2800
Connection ~ 6650 2800
$Comp
L Device:CP C11
U 1 1 5D342D8B
P 7900 1700
F 0 "C11" H 8018 1746 50  0000 L CNN
F 1 "10uF" H 8018 1655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 7938 1550 50  0001 C CNN
F 3 "~" H 7900 1700 50  0001 C CNN
	1    7900 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C12
U 1 1 5D343CD1
P 7900 2350
F 0 "C12" H 8018 2396 50  0000 L CNN
F 1 "10uF" H 8018 2305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 7938 2200 50  0001 C CNN
F 3 "~" H 7900 2350 50  0001 C CNN
	1    7900 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 1550 7900 1300
Wire Wire Line
	7900 1300 7750 1300
Wire Wire Line
	7900 1850 7900 2050
Wire Wire Line
	7450 2050 7900 2050
Connection ~ 7900 2050
Wire Wire Line
	7900 2050 7900 2200
Wire Wire Line
	7900 2500 7900 2800
Wire Wire Line
	7900 2800 7750 2800
Wire Wire Line
	3550 1650 3350 1650
Wire Wire Line
	3350 1650 3350 2450
Wire Wire Line
	3350 2450 3550 2450
Wire Wire Line
	3550 2800 3100 2800
Wire Wire Line
	3100 2800 3100 1300
Wire Wire Line
	3100 1300 3550 1300
Wire Wire Line
	2100 1300 3100 1300
Connection ~ 3100 1300
Wire Wire Line
	2100 1650 3350 1650
Connection ~ 3350 1650
Wire Wire Line
	6650 1300 7050 1300
Wire Wire Line
	6600 1150 6650 1150
Wire Wire Line
	6650 1150 6650 1300
Connection ~ 6650 1150
Wire Wire Line
	6650 1150 6700 1150
Wire Wire Line
	4300 2050 2400 2050
$Comp
L power:GND #PWR0101
U 1 1 5D3B3EF6
P 2400 2050
F 0 "#PWR0101" H 2400 1800 50  0001 C CNN
F 1 "GND" H 2405 1877 50  0000 C CNN
F 2 "" H 2400 2050 50  0001 C CNN
F 3 "" H 2400 2050 50  0001 C CNN
	1    2400 2050
	1    0    0    -1  
$EndComp
Connection ~ 2400 2050
Wire Wire Line
	2400 2050 2100 2050
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 5D3B635E
P 6600 850
F 0 "J4" V 6566 562 50  0000 R CNN
F 1 "+15V" V 6475 562 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6600 850 50  0001 C CNN
F 3 "~" H 6600 850 50  0001 C CNN
	1    6600 850 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6600 1050 6600 1150
Wire Wire Line
	6700 1050 6700 1150
Wire Wire Line
	6500 1050 6500 1150
Wire Wire Line
	6500 1150 6600 1150
Connection ~ 6600 1150
Wire Wire Line
	6700 1150 6800 1150
Wire Wire Line
	6800 1150 6800 1050
Connection ~ 6700 1150
Wire Wire Line
	7850 1150 7900 1150
Wire Wire Line
	7900 1150 7900 1300
Connection ~ 7900 1150
Wire Wire Line
	7900 1150 7950 1150
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 5D3C49D6
P 7850 850
F 0 "J5" V 7816 562 50  0000 R CNN
F 1 "+5V" V 7725 562 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7850 850 50  0001 C CNN
F 3 "~" H 7850 850 50  0001 C CNN
	1    7850 850 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 1050 7850 1150
Wire Wire Line
	7950 1050 7950 1150
Wire Wire Line
	7750 1050 7750 1150
Wire Wire Line
	7750 1150 7850 1150
Connection ~ 7850 1150
Wire Wire Line
	7950 1150 8050 1150
Wire Wire Line
	8050 1150 8050 1050
Connection ~ 7950 1150
Wire Wire Line
	6700 2950 6650 2950
Wire Wire Line
	6650 2950 6650 2800
Connection ~ 6650 2950
Wire Wire Line
	6650 2950 6600 2950
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 5D3CDD84
P 6700 3250
F 0 "J7" V 6573 3430 50  0000 L CNN
F 1 "-15V" V 6664 3430 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6700 3250 50  0001 C CNN
F 3 "~" H 6700 3250 50  0001 C CNN
	1    6700 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	6700 3050 6700 2950
Wire Wire Line
	6600 3050 6600 2950
Wire Wire Line
	6800 3050 6800 2950
Wire Wire Line
	6800 2950 6700 2950
Connection ~ 6700 2950
Wire Wire Line
	6600 2950 6500 2950
Wire Wire Line
	6500 2950 6500 3050
Connection ~ 6600 2950
Wire Wire Line
	7950 2950 7900 2950
Wire Wire Line
	7900 2950 7900 2800
Connection ~ 7900 2950
Wire Wire Line
	7900 2950 7850 2950
$Comp
L Connector_Generic:Conn_01x04 J8
U 1 1 5D3DACB1
P 7950 3250
F 0 "J8" V 7823 3430 50  0000 L CNN
F 1 "-5V" V 7914 3430 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 3250 50  0001 C CNN
F 3 "~" H 7950 3250 50  0001 C CNN
	1    7950 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 3050 7950 2950
Wire Wire Line
	7850 3050 7850 2950
Wire Wire Line
	8050 3050 8050 2950
Wire Wire Line
	8050 2950 7950 2950
Connection ~ 7950 2950
Wire Wire Line
	7850 2950 7750 2950
Wire Wire Line
	7750 2950 7750 3050
Connection ~ 7850 2950
Connection ~ 7900 2800
$Comp
L Connector_Generic:Conn_01x08 J6
U 1 1 5D3EA1A2
P 9750 2000
F 0 "J6" H 9829 1992 50  0000 L CNN
F 1 "GND" H 9829 1901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 9750 2000 50  0001 C CNN
F 3 "~" H 9750 2000 50  0001 C CNN
	1    9750 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 1700 9450 1700
Wire Wire Line
	9450 1700 9450 1800
Wire Wire Line
	9450 2400 9550 2400
Wire Wire Line
	9550 2300 9450 2300
Connection ~ 9450 2300
Wire Wire Line
	9450 2300 9450 2400
Wire Wire Line
	9550 2200 9450 2200
Connection ~ 9450 2200
Wire Wire Line
	9450 2200 9450 2300
Wire Wire Line
	9550 2100 9450 2100
Connection ~ 9450 2100
Wire Wire Line
	9450 2100 9450 2200
Wire Wire Line
	9550 2000 9450 2000
Connection ~ 9450 2000
Wire Wire Line
	9450 2000 9450 2050
Wire Wire Line
	9550 1900 9450 1900
Connection ~ 9450 1900
Wire Wire Line
	9450 1900 9450 2000
Wire Wire Line
	9550 1800 9450 1800
Connection ~ 9450 1800
Wire Wire Line
	9450 1800 9450 1900
Connection ~ 9450 2050
Wire Wire Line
	9450 2050 9450 2100
Wire Wire Line
	6300 2400 6300 2450
Connection ~ 6300 2450
Wire Wire Line
	6300 1700 6300 1650
Connection ~ 6300 1650
$Comp
L Device:C C9
U 1 1 5D41C39C
P 7050 1700
F 0 "C9" H 7165 1746 50  0000 L CNN
F 1 "100nF" H 7165 1655 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 7088 1550 50  0001 C CNN
F 3 "~" H 7050 1700 50  0001 C CNN
	1    7050 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5D41C65A
P 7050 2450
F 0 "C10" H 7165 2496 50  0000 L CNN
F 1 "100nF" H 7165 2405 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 7088 2300 50  0001 C CNN
F 3 "~" H 7050 2450 50  0001 C CNN
	1    7050 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 1550 7050 1300
Connection ~ 7050 1300
Wire Wire Line
	7050 1300 7150 1300
Wire Wire Line
	7050 1850 7050 2050
Connection ~ 7050 2050
Wire Wire Line
	7050 2050 6650 2050
Wire Wire Line
	7050 2300 7050 2050
Wire Wire Line
	7050 2600 7050 2800
Connection ~ 7050 2800
Wire Wire Line
	7050 2800 6650 2800
$Comp
L Device:C C13
U 1 1 5D4369EE
P 8350 1700
F 0 "C13" H 8465 1746 50  0000 L CNN
F 1 "100nF" H 8465 1655 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 8388 1550 50  0001 C CNN
F 3 "~" H 8350 1700 50  0001 C CNN
	1    8350 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 5D43B2C0
P 8350 2350
F 0 "C14" H 8465 2396 50  0000 L CNN
F 1 "100nF" H 8465 2305 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 8388 2200 50  0001 C CNN
F 3 "~" H 8350 2350 50  0001 C CNN
	1    8350 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1300 8350 1550
Wire Wire Line
	7900 1300 8350 1300
Connection ~ 7900 1300
Wire Wire Line
	8350 1850 8350 2050
Wire Wire Line
	7900 2050 8350 2050
Connection ~ 8350 2050
Wire Wire Line
	8350 2050 8350 2200
Wire Wire Line
	8350 2500 8350 2800
Wire Wire Line
	8350 2800 7900 2800
Connection ~ 8350 1300
$Comp
L Device:LED LED1
U 1 1 5D486BED
P 9150 1500
F 0 "LED1" V 9188 1382 50  0000 R CNN
F 1 "LED" V 9097 1382 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P3.81mm_Drill1mm" H 9150 1500 50  0001 C CNN
F 3 "~" H 9150 1500 50  0001 C CNN
	1    9150 1500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D487BA5
P 9150 1850
F 0 "R3" H 9220 1896 50  0000 L CNN
F 1 "560" H 9220 1805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9080 1850 50  0001 C CNN
F 3 "~" H 9150 1850 50  0001 C CNN
	1    9150 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 1650 9150 1700
Wire Wire Line
	9150 2000 9150 2050
Connection ~ 9150 2050
Wire Wire Line
	9150 2050 9450 2050
Wire Wire Line
	9150 1300 9150 1350
Wire Wire Line
	8350 1300 9150 1300
Wire Wire Line
	8350 2050 9150 2050
$EndSCHEMATC
